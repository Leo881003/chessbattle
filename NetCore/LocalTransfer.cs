﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace NetCore
{
	public class LocalTransfer : ITransferFrame, IClientFrame, IDisposable
	{
		public LocalTransfer()
		{

		}

		internal override bool AlwaysReceive
		{
			get
			{
				return true;
			}
			set
			{
				return;
			}
		}
		public override bool Connected
		{
			get
			{
				if (Received.GetInvocationList().Length == 0)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			internal set
			{
				return;
			}
		}
		public bool ClientConnected
		{
			get
			{
				return Connected;
			}
		}
		internal override event ReceiveEventHandler Received;

		internal override void Receive()
		{
			return; //it's no use to call begin receive.
		}

		internal override void Send(byte[] data)
		{
			ReceiveEventArgs arg = new ReceiveEventArgs(data.Length);
			arg.Data = data;
            try
            {
                ClientReceived.Invoke(arg);
            }
            catch (NullReferenceException ex)
            {
                DebugControl.PrintError(ex);
            }
		}
		//Client Method---vvv
		public void ClientSend(byte[] data)
		{
			ReceiveEventArgs arg = new ReceiveEventArgs(data.Length);
			arg.Data = data;
            try
            {
                Received.Invoke(arg);
            }
            catch (NullReferenceException ex)
            {
                DebugControl.PrintError(ex);
            }
        }
		
		public void Connect(EndPoint ep)
		{
			return;
		}    
        public event ReceiveEventHandler ClientReceived;
        public override event None TransferError;

        #region DisposeBlock
        private bool disposed = false;
        public override void Dispose()
        {
            Dispose(true);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                disposed = true;
            }
        }
        ~LocalTransfer()
        {
            Dispose(false);
        }
        #endregion
    }
}
