﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChessCore;

namespace NetCore
{
    public static class PackageManager
    {
        /*Template
        public static byte[] a()
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.YourTurn);
            return ls.ToArray();
        }
        */
        public static byte[] GetPanel()
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.GetPanel);
            return ls.ToArray();
        }
        public static byte[] SendPanel(ChessPanel Panel)
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.SendPanel);
            ls.AddRange(Panel.GetBytes());
            return ls.ToArray();
        }
        public static byte[] YourTurn()
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.YourTurn);
            return ls.ToArray();
        }
        public static byte[] Move(XY from,XY to)
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.Move);
            ls.AddRange(BitConverter.GetBytes(from.X));
            ls.AddRange(BitConverter.GetBytes(from.Y));
            ls.AddRange(BitConverter.GetBytes(to.X));
            ls.AddRange(BitConverter.GetBytes(to.Y));
            return ls.ToArray();
        }
        public static byte[] Upgrade()
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.Upgrade);
            return ls.ToArray();
        }
        public static byte[] Upgrade(UpgradeType type)
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.Upgrade);
            ls.Add((byte)type);
            return ls.ToArray();
        }
        public static byte[] GameOver(GameOverType type)
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.GameOver);
            ls.AddRange(BitConverter.GetBytes((byte)type));
            return ls.ToArray();
        }
        public static byte[] DrawAccept()
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.DrawAccept);
            return ls.ToArray();
        }
        public static byte[] DrawDeny()
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.DrawDeny);
            return ls.ToArray();
        }
        public static byte[] Chat(ChatType type,string Text)
        {
            UnicodeEncoding enc = new UnicodeEncoding();
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.Chat);
            ls.Add((byte)type);
            ls.AddRange(enc.GetBytes(Text));
            return ls.ToArray();
        }
        public static byte[] Surrend()
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.Surrend);
            return ls.ToArray();
        }
        public static byte[] GameData(ChessOwner owner)
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.GameData);
            ls.Add((byte)owner);
            return ls.ToArray();
        }
        public static byte[] InvaildOperate()
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.InvaildOperate);
            return ls.ToArray();
        }
        public static byte[] Serrch()
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.Search);
            return ls.ToArray();
        }
        public static byte[] Reply()
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.Reply);
            return ls.ToArray();
        }
        public static byte[] GameAccept(bool First = false)
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.GameAccept);
            ls.AddRange(BitConverter.GetBytes(First));
            return ls.ToArray();
        }
        public static byte[] GameDeny()
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.GameDeny);
            return ls.ToArray();
        }
        public static byte[] UserData()
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.UserData);
            //TODO:AddUserData
            return ls.ToArray();
        }
        public static byte[] WaitConnect(bool Complete = false)
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.WaitConnect);
            ls.AddRange(BitConverter.GetBytes(Complete));
            return ls.ToArray();
        }
        public static byte[] ConnectAuth()//long PassParse)
        {
            List<byte> ls = new List<byte>();
            ls.Add((byte)PackType.ConnectAuth);
            //ls.AddRange(BitConverter.GetBytes(PassParse));
            return ls.ToArray();
        }
    }   
}
