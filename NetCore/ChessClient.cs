﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using ChessCore;

namespace NetCore
{
    public class ChessClient : IDisposable
    {
        IClientFrame trans;
        public ChessOwner Own
        {
            get;
            private set;
        }
        public ChessClient(IClientFrame tranframe, EndPoint ep)
        {
            trans = tranframe;
            try
            {
                trans.Connect(ep);
                trans.ClientReceived += Received;
                trans.TransferError += Trans_TransferError;
            }
            catch (Exception)
            {
                CallErrorDia();
            }
        }

        private void Trans_TransferError()
        {
            CallErrorDia();
        }
        private void CallErrorDia()
        {
            try
            {
                SocketError.Invoke(this);
            }
            catch (NullReferenceException ex)
            {
                DebugControl.PrintError(ex);
            }
        }

        public event Simple SocketError;
        //----------------------------
        public event MoveEvent Move;
        public event DrawEvent Draw;
        public event OverEvent GameOver;
        public event Simple Turn;
        public event Simple Error;
        public event Simple UpgradeDia;
        public event UpgradeEvent Upgrade;
        public event Simple OwnGet;
        private void Received(ReceiveEventArgs e)
        {
            try
            {
                switch ((PackType)e.Data[0])
                {
                    case PackType.SendPanel:
                        break;
                    case PackType.YourTurn:
                        Turn(this);
                        break;
                    case PackType.Move:
                        Move(this, BitConverter.ToInt32(e.Data, 1), BitConverter.ToInt32(e.Data, 5), BitConverter.ToInt32(e.Data, 9), BitConverter.ToInt32(e.Data, 13));
                        break;
                    case PackType.Upgrade:
                        if (e.Size == 2)
                        {
                            Upgrade(this, (UpgradeType)e.Data[1]);
                        }
                        else if (e.Size == 1)
                        {
                            UpgradeDia(this);
                        }
                        break;
                    case PackType.GameOver:
                        GameOver(this, (GameOverType)e.Data[1]);
                        break;
                    case PackType.DrawAccept:
                        Draw(this, true);
                        break;
                    case PackType.DrawDeny:
                        Draw(this, false);
                        break;
                    case PackType.Chat:
                        break;
                    case PackType.GameData:
                        Own = (ChessOwner)e.Data[1];
                        OwnGet(this);
                        break;
                    case PackType.InvaildOperate:
                        Error(this);
                        break;
                }
            }
            catch (NullReferenceException ex)
            {
                DebugControl.PrintError(ex);
            }
        }
        public void SendUpgrade(UpgradeType type)
        {
            trans.ClientSend(PackageManager.Upgrade(type));
        }
        public void SendDraw(bool Agree)
        {
            if (Agree)
            {
                trans.ClientSend(PackageManager.DrawAccept());
            }
            else
            {
                trans.ClientSend(PackageManager.DrawDeny());
            }
        }
        public void SendSurrend()
        {
            trans.ClientSend(PackageManager.Surrend());
        }
        public void SendMove(XY from, XY to)
        {
            trans.ClientSend(PackageManager.Move(from, to));
        }
        public void SendOk()
        {
            trans.ClientSend(PackageManager.ConnectAuth());
        }

        #region DisposeBlock
        private bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                trans.Dispose();
                disposed = true;
            }
        }
        ~ChessClient()
        {
            Dispose(false);
        }
        #endregion
    }
    public delegate void UpgradeEvent(ChessClient sender, UpgradeType type);
    public delegate void MoveEvent(ChessClient sender, int fx, int fy, int tx, int ty);
    public delegate void DrawEvent(ChessClient sender, bool accept);
    public delegate void OverEvent(ChessClient sender, GameOverType type);
    public delegate void Simple(ChessClient sender);
}
