﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Threading;
using System.Linq;
using System.Text;
using ChessCore;

namespace NetCore
{
    public class ChessServer : IDisposable
    {
        ITransferFrame black;
        ITransferFrame white;
        Timer sectimer;
        Thread main;
        ChessPanel panel = new ChessPanel();
        bool connected = false;
        bool whitedraw = false;
        bool blackdraw = false;
        bool whiteok = false;
        bool blackok = false;
        TurnType tu = TurnType.WhiteBegin;
        private TurnType Turn
        {
            get
            {
                return tu;
            }
            set
            {
                tu = value;
                if(tu == TurnType.Stop)
                {
                    Dispose();
                }
            }
        }

        public ChessServer(ITransferFrame black, ITransferFrame white)
        {
            this.black = black;
            this.white = white;
            sectimer = new Timer(new TimerCallback(ChkConn), null, 0, 500);
            main = new Thread(new ThreadStart(Processer));
            main.Priority = ThreadPriority.AboveNormal;
            main.IsBackground = true;
            this.white.AlwaysReceive = true;
            this.black.AlwaysReceive = true;
            this.white.Received += White_Rec;
            this.black.Received += Black_Rec;
            this.white.TransferError += White_TransferError;
            this.black.TransferError += Black_TransferError;
        }

        private void Black_TransferError()
        {
            white.Send(PackageManager.GameOver(GameOverType.Disconnect));
            Turn = TurnType.Stop;
        }
        private void White_TransferError()
        {
            black.Send(PackageManager.GameOver(GameOverType.Disconnect));
            Turn = TurnType.Stop;
        }

        private void ChkConn(object sender)
        {
            if (black.Connected && white.Connected)
            {
                connected = true;
                sectimer.Change(Timeout.Infinite, Timeout.Infinite);
                white.Send(PackageManager.GameData(ChessOwner.White));
                black.Send(PackageManager.GameData(ChessOwner.Black));
                white.Send(PackageManager.WaitConnect(true));
                black.Send(PackageManager.WaitConnect(true));
                main.Start();
            }
        }
        private void Processer()
        {
            try
            {
                while (true)
                {
                    if (whiteok && blackok)
                    {
                        break;
                    }
                    else
                    {
                        Thread.Sleep(100);
                    }
                }
                Next();
            }
            catch (SocketException ex)
            {
                Debug.Print("SocketException:\n{0}", ex);
            }
            catch (Exception ex)
            {
                Debug.Print("UnknownException:\n{0}:\n{1}", new object[] { ex.GetType().FullName, ex });
            }
        }
        private void Next()
        {
            switch (Turn)
            {
                case TurnType.WhiteBegin:
                    Turn = TurnType.WhiteDone;
                    white.Send(PackageManager.YourTurn());
                    break;
                case TurnType.WhiteDone:
                    Turn = TurnType.BlackBegin;
                    CheckGameOver(ChessOwner.Black);
                    Next();
                    break;
                case TurnType.BlackBegin:
                    Turn = TurnType.BlackDone;
                    black.Send(PackageManager.YourTurn());
                    break;
                case TurnType.BlackDone:
                    Turn = TurnType.WhiteBegin;
                    CheckGameOver(ChessOwner.White);
                    Next();
                    break;
            }
        }
        private void White_Rec(ReceiveEventArgs e)
        {
            PackProc(true, e.Data);
        }
        private void Black_Rec(ReceiveEventArgs e)
        {
            PackProc(false, e.Data);
        }
        private bool CheckGameOver(ChessOwner owner)
        {
            if (owner == ChessOwner.White)
            {
                switch (panel.CheckMate(ChessOwner.White))
                {
                    case CheckMateType.CheckMate:
                        white.Send(PackageManager.GameOver(GameOverType.BlackWin));
                        black.Send(PackageManager.GameOver(GameOverType.BlackWin));
                        Turn = TurnType.Stop;
                        return true;
                    case CheckMateType.Draw:
                        white.Send(PackageManager.GameOver(GameOverType.Draw));
                        black.Send(PackageManager.GameOver(GameOverType.Draw));
                        Turn = TurnType.Stop;
                        return true;
                }
            }
            if (owner == ChessOwner.Black)
            {
                switch (panel.CheckMate(ChessOwner.Black))
                {
                    case CheckMateType.CheckMate:
                        white.Send(PackageManager.GameOver(GameOverType.WhiteWin));
                        black.Send(PackageManager.GameOver(GameOverType.WhiteWin));
                        Turn = TurnType.Stop;
                        return true;
                    case CheckMateType.Draw:
                        white.Send(PackageManager.GameOver(GameOverType.Draw));
                        black.Send(PackageManager.GameOver(GameOverType.Draw));
                        Turn = TurnType.Stop;
                        return true;
                }
            }
            return false;
        }
        private void PackProc(bool Iswhite, params byte[] data)
        {
            try
            {
                switch ((PackType)data[0])
                {
                    case PackType.GetPanel:
                        break;
                    case PackType.Move:
                        #region Move                        
                        if (Iswhite)
                        {
                            if (Turn != TurnType.WhiteDone)
                            {
                                white.Send(PackageManager.InvaildOperate());
                                break;
                            }
                        }
                        else
                        {
                            if (Turn != TurnType.BlackDone)
                            {
                                black.Send(PackageManager.InvaildOperate());
                                break;
                            }
                        }
                        int fromx = BitConverter.ToInt32(data, 1);
                        int fromy = BitConverter.ToInt32(data, 5);
                        int tox = BitConverter.ToInt32(data, 9);
                        int toy = BitConverter.ToInt32(data, 13);
                        if ((Iswhite ? ChessOwner.White : ChessOwner.Black) == panel[fromx, fromy].Owner)
                        {
                            if (!panel.Move(new XY(fromx, fromy), new XY(tox, toy)))
                            {
                                (Iswhite ? white : black).Send(PackageManager.InvaildOperate());
                                break;
                            }
                        }
                        else
                        {
                            (Iswhite ? white : black).Send(PackageManager.InvaildOperate());
                            break;
                        }
                        black.Send(data);
                        white.Send(data);
                        if (panel.Upgrading != ChessOwner.None)
                        {
                            (Iswhite ? white : black).Send(PackageManager.Upgrade());
                            break;
                        }
                        Next();
                        #endregion
                        break;
                    case PackType.Upgrade:
                        panel.Upgrade((UpgradeType)data[1]);
                        black.Send(data);
                        white.Send(data);
                        Next();
                        break;
                    case PackType.DrawAccept:
                        if (Iswhite) whitedraw = true;
                        else blackdraw = true;
                        if (blackdraw & whitedraw == true)
                        {
                            white.Send(PackageManager.GameOver(GameOverType.Draw));
                            black.Send(PackageManager.GameOver(GameOverType.Draw));
                            Turn = TurnType.Stop;
                        }
                        else
                        {
                            (blackdraw ? white : black).Send(PackageManager.DrawAccept());
                        }
                        break;
                    case PackType.DrawDeny:
                        if ((Iswhite ? whitedraw : blackdraw) == false && (!Iswhite ? whitedraw : blackdraw) == true)
                        {
                            (!Iswhite ? white : black).Send(PackageManager.DrawDeny());
                        }
                        break;
                    case PackType.Chat:
                        break;
                    case PackType.Surrend:
                        white.Send(PackageManager.GameOver((Iswhite ? GameOverType.WhiteSurrend : GameOverType.BlackSurrend)));
                        black.Send(PackageManager.GameOver((Iswhite ? GameOverType.WhiteSurrend : GameOverType.BlackSurrend)));
                        Turn = TurnType.Stop;
                        break;
                    case PackType.ConnectAuth:
                        if (Iswhite) whiteok = true;
                        else blackok = true;
                        break;
                    default:
                        (Iswhite ? white : black).Send(PackageManager.InvaildOperate());
                        break;
                }
            }
            catch (Exception ex)
            {
                Debug.Print("Exception:\n{0}:\n{1}", new object[] { ex.GetType().FullName, ex });
            }
        }
        #region DisposeBlock
        private bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
        }
        protected virtual void Dispose(bool disposing)
        {
            if(!disposed)
            {
                if(disposing)
                {
                    panel = null;
                    sectimer.Dispose();
                }
                white.Dispose();
                black.Dispose();
                disposed = true;
            }
        }
        ~ChessServer()
        {
            Dispose(false);
        }
        #endregion       
    }
}
