﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCore
{
    public abstract class ITransferFrame : IDisposable
    {
        internal abstract void Send(byte[] data);
        internal abstract void Receive();
        public abstract void Dispose();

        internal abstract event ReceiveEventHandler Received;
        public abstract event None TransferError;
        public abstract bool Connected
        {
            get;
            internal set;
        }
        internal abstract bool AlwaysReceive
        {
            get;
            set;
        }
    }

    public interface IClientFrame : IDisposable
    {
        bool ClientConnected
        {
            get;
        }
        void ClientSend(byte[] data);
        event ReceiveEventHandler ClientReceived;
        event None TransferError;
        void Connect(System.Net.EndPoint ep);
    }
}
