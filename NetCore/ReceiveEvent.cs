﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCore
{
    public delegate void ReceiveEventHandler(ReceiveEventArgs e);
    public class ReceiveEventArgs
    {
        public byte[] Data;
        public readonly int Size;
        public ReceiveEventArgs(int length)
        {
            Data = new byte[length];
            Size = length;
        }
    }
}
