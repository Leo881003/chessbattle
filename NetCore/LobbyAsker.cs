﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace NetCore
{
    public delegate void None();
    public class LobbyAsker
    {
        List<IPAddress> ls = new List<IPAddress>();
        Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        IPEndPoint broad = new IPEndPoint(IPAddress.Broadcast, 3838);
        IPEndPoint local = new IPEndPoint(IPAddress.Any, 0);
        bool asking = false;

        public event None Completed;
        public LobbyAsker()
        {
            s.Bind(local);
            s.ReceiveTimeout = 5000;
        }
        public void BeginAsk()
        {
            ls.Clear();
            try
            {
                byte[] sendbuff = new byte[5];
                BitConverter.GetBytes(1).CopyTo(sendbuff, 0);
                sendbuff[4] = (byte)PackType.Search;
                s.BeginSendTo(sendbuff, 0, sendbuff.Length, SocketFlags.Broadcast, broad, new AsyncCallback(SendCallBack), s);
                asking = true;
            }
            catch (SocketException)
            {
                throw;
            }
        }
        private void SendCallBack(IAsyncResult ar)
        {
            s.EndSendTo(ar);
            Receive();
        }
        private void Receive()
        {
            EndPoint tmp = new IPEndPoint(IPAddress.Any, 0);
            byte[] buff = new byte[5];
            s.BeginReceiveFrom(buff, 0, buff.Length, SocketFlags.None, ref tmp, new AsyncCallback(ReceiveCallBack), buff);
        }
        private void ReceiveCallBack(IAsyncResult ar)
        {
            EndPoint source = new IPEndPoint(IPAddress.Any, 0);
            try
            {
                s.EndReceiveFrom(ar, ref source);
            }
            catch(SocketException ex)
            {
                if(ex.SocketErrorCode == SocketError.TimedOut)
                {
                    asking = false;
                    Completed.Invoke();
                    return;
                }
            }
            byte[] buff = (byte[])ar.AsyncState;
            if (BitConverter.ToInt32(buff, 0) == 1)
            {
                if((PackType)buff[4] == PackType.Reply)
                {
                    ls.Add(((IPEndPoint)source).Address);
                }
            }
            Receive();
        }
        public List<IPAddress> EndAsking()
        {
            if (!asking)
            {
                return ls;
            }
            else return null;
        }
    }
}
