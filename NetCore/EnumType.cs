﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCore
{
    public enum TurnType
    {
        WhiteBegin,
        WhiteDone,
        BlackBegin,
        BlackDone,
        Stop
    }
    public enum ChatType : byte
    {
        Player,
        Self,
        System
    }
    public enum GameOverType : byte
    {
        WhiteWin,
        BlackWin,
        BlackSurrend,
        WhiteSurrend,
        Draw,
        Disconnect
    }
}
