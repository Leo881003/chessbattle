﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCore
{
    public class DebugControl
    {
        public static void PrintError(Exception ex)
        {
            Debug.Print(ex.GetType().Name + ":\n" + ex.ToString());
        }
        public static void StartDebugWindows()
        {
            TextWriterTraceListener writer = new TextWriterTraceListener(Console.Out);
            Debug.Listeners.Add(writer);            
        }
    }
}
