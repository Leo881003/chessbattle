﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace NetCore
{
    public class ClientTransfer : IClientFrame, IDisposable
    {
        byte[] buff = new byte[1024];
        Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private bool connected = false;
        public ClientTransfer()
        {

        }
        public void Connect(EndPoint ep)
        {
            try
            {
                s.BeginConnect(ep, new AsyncCallback(EndConnect), s);
            }
            catch (SocketException ex)
            {
                if (NetTransfer.ErrorHandle(ex))
                {
                    CallError();
                }
            }
            catch (ObjectDisposedException)
            {
                return;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void EndConnect(IAsyncResult ar)
        {
            try
            {
                s.EndConnect(ar);
                connected = true;
                Receive();
                try
                {
                    Accepted();
                }
                catch (NullReferenceException ex)
                {
                    DebugControl.PrintError(ex);
                }
            }
            catch (SocketException ex)
            {
                Debug.Print("SocketException:\n{0}", ex);
            }
            catch (ObjectDisposedException)
            {
                return;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public event None Accepted;
        public event ReceiveEventHandler ClientReceived;
        public event None TransferError;

        private void Receive()
        {
            if (!connected)
            {
                throw new InvalidOperationException();
            }
            try
            {
                s.BeginReceive(buff, 0, 4, SocketFlags.None, new AsyncCallback(SizeCallBack), s);
            }
            catch (SocketException ex)
            {
                if (NetTransfer.ErrorHandle(ex))
                {
                    CallError();                  
                }
                Receive();
            }
            catch (ObjectDisposedException)
            {
                return;
            }
            catch (ArgumentNullException)
            {
                return;
            }
        }
        private void SizeCallBack(IAsyncResult ar)
        {
            try
            {
                s.EndReceive(ar);
                int size = BitConverter.ToInt32(buff, 0);
                s.BeginReceive(buff, 0, size, SocketFlags.None, new AsyncCallback(RecCallBack), size);
            }
            catch (SocketException ex)
            {
                Debug.Print("SocketException:\n{0}", ex);
                Receive();
            }
            catch (ObjectDisposedException)
            {
                return;
            }
            catch (ArgumentNullException)
            {
                return;
            }
        }
        private void RecCallBack(IAsyncResult ar)
        {
            try
            {
                s.EndReceive(ar);
                ReceiveEventArgs arg = new ReceiveEventArgs((int)ar.AsyncState);
                Array.Copy(buff, arg.Data, arg.Size);
                try
                {
                    ClientReceived.Invoke(arg);
                }
                catch (NullReferenceException ex)
                {
                    DebugControl.PrintError(ex);
                }
                //ClientReceived.BeginInvoke(arg,new AsyncCallback(RecEventCallBack),null);
            }
            catch (SocketException ex)
            {
                if (NetTransfer.ErrorHandle(ex))
                {
                    CallError();
                }
            }
            catch (ObjectDisposedException)
            {
                return;
            }
            catch (ArgumentNullException)
            {
                return;
            }
            finally
            {
                Receive();
            }
        }
        private void RecEventCallBack(IAsyncResult ar)
        {
            ClientReceived.EndInvoke(ar);
        }
        public bool ClientConnected
        {
            get
            {
                return connected;
            }
        }

        public void ClientSend(byte[] data)
        {
            if (!connected)
            {
                for (int i = 0; i < 10; i++)
                {
                    Thread.Sleep(100);
                    if (connected) goto retryok;
                }
                throw new SocketException((int)SocketError.TimedOut);
            }
            retryok:
            try
            {
                byte[] send = new byte[data.Length + 4];
                BitConverter.GetBytes(data.Length).CopyTo(send, 0);
                Array.Copy(data, 0, send, 4, data.Length);
                s.BeginSend(send, 0, send.Length, SocketFlags.None, new AsyncCallback(SendCallBack), s);
            }
            catch (SocketException ex)
            {
                if (NetTransfer.ErrorHandle(ex))
                {
                    CallError();
                }
            }
            catch (ObjectDisposedException)
            {
                return;
            }
            catch (ArgumentNullException)
            {
                return;
            }
        }
        private void SendCallBack(IAsyncResult ar)
        {
            try
            {
                s.EndSend(ar);
            }
            catch (SocketException ex)
            {
                if (NetTransfer.ErrorHandle(ex))
                {
                    CallError();
                }
            }
            catch (ObjectDisposedException)
            {
                return;
            }
            catch (ArgumentNullException)
            {
                return;
            }
        }
        private void CallError()
        {
            try
            {
                TransferError.Invoke();
            }
            catch (NullReferenceException ex)
            {
                DebugControl.PrintError(ex);
            }
        }

        #region DisposeBlock
        private bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    buff = null;
                }
                try
                {
                    if (s.Connected) s.Disconnect(false);
                    else s.Dispose();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
        }
        ~ClientTransfer()
        {
            Dispose(false);
        }
        #endregion
    }
}
