﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCore
{
    public enum PackType : byte
    {
        //0x00:GameOperate
        GetPanel = 0x00,
        SendPanel = 0x01,
        YourTurn = 0x02,
        Move = 0x03,
        Upgrade = 0x04,
        GameOver = 0x09,
        DrawAccept = 0x0A,
        DrawDeny = 0x0B,
        Chat = 0x0C,
        Surrend = 0x0D,
        GameData = 0x0E,
        InvaildOperate = 0x0F,
        //0xA0:AuthenticOperate
        Search = 0xA0,
        Reply = 0xA1,
        GameAccept = 0xA2,
        GameDeny = 0xA3,
        UserData = 0xA4,
        WaitConnect = 0xA5,
        ConnectAuth = 0xA6,
    }
}
