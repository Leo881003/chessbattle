﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace NetCore
{
    public class NetTransfer : ITransferFrame, IDisposable
    {
        byte[] buff = new byte[1024];
        Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        IPEndPoint local = new IPEndPoint(IPAddress.Any, 7500);

        public override bool Connected
        {
            get;
            internal set;
        }
        internal override bool AlwaysReceive
        {
            get;
            set;
        }

        internal override event ReceiveEventHandler Received;
        public NetTransfer()
        {
            try
            {
                listener.Bind(local);
                listener.Listen(1);
                listener.BeginAccept(new AsyncCallback(AcceptCallBack), listener);
            }
            catch (SocketException ex)
            {
                Debug.Print("SocketException:\n{0}", ex);
            }
            catch (ObjectDisposedException)
            {
                return;
            }
        }
        private void AcceptCallBack(IAsyncResult ar)
        {
            try
            {
                s = listener.EndAccept(ar);
                s.SendTimeout = 5000;
                //TODO:AddAuth
                Connected = true;
                if (AlwaysReceive) Receive();
                try
                {
                    Accepted();
                }
                catch (NullReferenceException ex)
                {
                    DebugControl.PrintError(ex);
                }               
            }
            catch (SocketException ex)
            {
                Debug.Print("SocketException:\n{0}", ex);
            }
            catch (ObjectDisposedException)
            {
                return;
            }
        }
        public event None Accepted;
        public override event None TransferError;

        internal override void Receive()
        {
            if (!Connected)
            {
                throw new InvalidOperationException();
            }
            try
            {
                s.BeginReceive(buff, 0, 4, SocketFlags.None, new AsyncCallback(SizeCallBack), s);
            }
            catch (SocketException ex)
            {
                if (NetTransfer.ErrorHandle(ex))
                {
                    if (NetTransfer.ErrorHandle(ex))
                    {
                        CallError();
                    }
                }
                Receive();
            }
            catch (ObjectDisposedException)
            {
                return;
            }
            catch (ArgumentNullException)
            {
                return;
            }
        }
        private void SizeCallBack(IAsyncResult ar)
        {
            try
            {
                s.EndReceive(ar);
                int size = BitConverter.ToInt32(buff, 0);
                s.BeginReceive(buff, 0, size, SocketFlags.None, new AsyncCallback(RecCallBack), size);
            }
            catch (SocketException ex)
            {
                Debug.Print("SocketException:\n{0}", ex);
                if (AlwaysReceive) Receive();
            }
            catch (ObjectDisposedException)
            {
                return;
            }
            catch (ArgumentNullException)
            {
                return;
            }
        }
        private void RecCallBack(IAsyncResult ar)
        {
            try
            {
                s.EndReceive(ar);
                ReceiveEventArgs arg = new ReceiveEventArgs((int)ar.AsyncState);
                Array.Copy(buff, arg.Data, arg.Size);
                try
                {
                    Received.Invoke(arg);
                }
                catch (NullReferenceException ex)
                {
                    DebugControl.PrintError(ex);
                }
            }
            catch (SocketException ex)
            {
                if (NetTransfer.ErrorHandle(ex))
                {
                    if (NetTransfer.ErrorHandle(ex))
                    {
                        CallError();
                    }
                }
            }
            catch (ObjectDisposedException)
            {
                return;
            }
            catch (ArgumentNullException)
            {
                return;
            }
            finally
            {
                if (AlwaysReceive) Receive();
            }
        }
        internal override void Send(byte[] data)
        {
            if (!Connected)
            {
                throw new InvalidOperationException();
            }
            try
            {
                byte[] send = new byte[data.Length + 4];
                BitConverter.GetBytes(data.Length).CopyTo(send, 0);
                Array.Copy(data, 0, send, 4, data.Length);
                s.BeginSend(send, 0, send.Length, SocketFlags.None, new AsyncCallback(SendCallBack), s);
            }
            catch (SocketException ex)
            {
                if (NetTransfer.ErrorHandle(ex))
                {
                    if (NetTransfer.ErrorHandle(ex))
                    {
                        CallError();
                    }
                }
            }
            catch (ObjectDisposedException)
            {
                return;
            }
            catch (ArgumentNullException)
            {
                return;
            }
        }
        private void SendCallBack(IAsyncResult ar)
        {
            try
            {
                s.EndSend(ar);
            }
            catch (SocketException ex)
            {
                if (NetTransfer.ErrorHandle(ex))
                {
                    if (NetTransfer.ErrorHandle(ex))
                    {
                        CallError();
                    }
                }
            }
            catch (ObjectDisposedException)
            {
                return;
            }
            catch (ArgumentNullException)
            {
                return;
            }
        }
        private void CallError()
        {
            try
            {
                TransferError.Invoke();
            }
            catch (NullReferenceException ex)
            {
                DebugControl.PrintError(ex);
            }
        }

        public static bool ErrorHandle(SocketException ex)
        {
            Debug.Print("SocketException:\n{0}", ex);
            switch (ex.SocketErrorCode)
            {
                case SocketError.Shutdown:
                case SocketError.NetworkDown:
                case SocketError.ConnectionRefused:
                case SocketError.ConnectionReset:
                case SocketError.HostDown:
                case SocketError.Disconnecting:
                    return true;
            }
            return false;
        }

        #region DisposeBlock
        private bool disposed = false;
        public override void Dispose()
        {
            Dispose(true);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    buff = null;
                }
                try
                {
                    listener.Dispose();
                    if (s.Connected) s.Disconnect(false);
                    else s.Dispose();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
                disposed = true;
            }
        }
        ~NetTransfer()
        {
            Dispose(false);
        }
        #endregion
    }
}
