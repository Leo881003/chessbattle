﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace NetCore
{
    public class LobbyListener
    {
        bool started = false;
        bool closed = false;
        Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        byte[] buff = new byte[1024];
        IPEndPoint local = new IPEndPoint(IPAddress.Any, 3838);
        public void Start()
        {
            if (started) return;
            if (closed)
            {
                s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                closed = false;
            }
            s.SendTimeout = 3000;
            try
            {
                s.Bind(local);
                EndPoint refep = (EndPoint)local;
                s.BeginReceiveFrom(buff, 0, 1024, SocketFlags.Broadcast, ref refep, new AsyncCallback(ReceivePack), s);
            }
            catch (SocketException)
            {
            }
        }
        public void Stop()
        {
            if (closed) return;
            try
            {
                s.Close();
                closed = true;
                started = false;
            }
            catch (SocketException)
            {
            }
        }
        private void ReceivePack(IAsyncResult ar)
        {
            try
            {
                EndPoint refep = (EndPoint)local;
                s.EndReceiveFrom(ar, ref refep);
                int size = BitConverter.ToInt32(buff, 0);
                if ((PackType)buff[4] == PackType.Search)
                {
                    byte[] sendbuff = new byte[5];
                    BitConverter.GetBytes(1).CopyTo(sendbuff, 0);
                    sendbuff[4] = (byte)PackType.Reply;
                    s.BeginSendTo(sendbuff, 0, sendbuff.Length, SocketFlags.None, refep, new AsyncCallback(SendCallBack), s);
                }
            }
            catch (ObjectDisposedException)
            {
                return;
            }
            catch (SocketException)
            {
            }
            finally
            {
                Start();
            }
        }
        private void SendCallBack(IAsyncResult ar)
        {
            s.EndSendTo(ar);
        }
    }
}
