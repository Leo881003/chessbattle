﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ChessCore;

namespace ChessBattle
{
    /// <summary>
    /// UpgradeDialog.xaml 的互動邏輯
    /// </summary>
    public partial class UpgradeDialog : Window
    {
        private ChessOwner owner;
        public UpgradeDialog()
        {
            InitializeComponent();
            Who = ChessOwner.White;
        }
        public new UpgradeType ShowDialog()
        {
            base.ShowDialog();
            return Type;
        }
        public ChessOwner Who
        {
            get
            {
                return owner;
            }
            set
            {
                if (value == ChessOwner.None) return;
                owner = value;
                switch (owner)
                {
                    case ChessOwner.White:
                        Q.BackColor = Colors.DimGray;
                        B.BackColor = Colors.DimGray;
                        N.BackColor = Colors.DimGray;
                        R.BackColor = Colors.DimGray;
                        Q.Chess = ChessType.WhiteQueen;
                        B.Chess = ChessType.WhiteBishop;
                        N.Chess = ChessType.WhiteKnight;
                        R.Chess = ChessType.WhiteRook;
                        break;
                    case ChessOwner.Black:
                        Q.BackColor = Colors.WhiteSmoke;
                        B.BackColor = Colors.WhiteSmoke;
                        N.BackColor = Colors.WhiteSmoke;
                        R.BackColor = Colors.WhiteSmoke;
                        Q.Chess = ChessType.BlackQueen;
                        B.Chess = ChessType.BlackBishop;
                        N.Chess = ChessType.BlackKnight;
                        R.Chess = ChessType.BlackRook;
                        break;
                }
            }
        }
        public UpgradeType Type
        {
            get;
            private set;
        }

        private void Q_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Type = UpgradeType.Queen;
            Close();
        }
        private void B_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Type = UpgradeType.Bishop;
            Close();
        }
        private void N_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Type = UpgradeType.Knight;
            Close();
        }
        private void R_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Type = UpgradeType.Rook;
            Close();
        }
    }
}
