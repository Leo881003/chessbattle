﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ChessCore;
using NetCore;

namespace ChessBattle
{
    public partial class ChessPage : Page
    {
        private Block[,] b = new Block[8, 8];
        private ChessPanel p = new ChessPanel();
        private int selectx = -1;
        private int selecty = -1;
        private bool flip = true;
        private ChessClient client1;
        private ChessClient client2;
        private int clientcount = 0;
        private ChessOwner moving = ChessOwner.None;
        public new MainWindow Parent = MainWindow.mother;

        public ChessClient Client
        {
            private get;
            set;
        }
        public bool Isfilp
        {
            get
            {
                return flip;
            }
            set
            {
                if (flip == value) return;
                flip = value;
                Fliper();
            }
        }
        public ChessPage()
        {
            Visibility = Visibility.Hidden;
            UpdateLayout();
            InitializeComponent();
            Init();
            Visibility = Visibility.Visible;
            UpdateLayout();
        }
        public ChessPage(ChessClient client) : this()
        {
            client1 = client;
            clientcount = 1;
            if (client1.Own == ChessOwner.Black) Isfilp = false;
            client1.Move += Client_Move;
            client1.Turn += Client_Turn;
            client1.GameOver += Client_GameOver;
            client1.Draw += Client_Draw;
            client1.Error += Client_Error;
            client1.Upgrade += Client_Upgrade;
            client1.UpgradeDia += Client_UpgradeDia;
            client1.SocketError += Client_SocketError;
            client1.OwnGet += Client_OwnGet;
            client1.SendOk();            
        }

        private void Client_OwnGet(ChessClient sender)
        {
            if (clientcount == 2) return;
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (sender.Own == ChessOwner.Black) Isfilp = false;
            }));
        }

        private void Client_SocketError(ChessClient sender)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                YesNoDialog yn = new YesNoDialog();
                yn.Owner = Parent;
                yn.CaptionText = "連線錯誤";
                yn.ContentText = "連線發生錯誤";
                yn.OKOnly = true;
                yn.ShowDialog();
                Parent.MainMenu();
            }));
            client1.Dispose();
        }

        public ChessPage(ChessClient first, ChessClient second) : this()
        {
            client1 = first;
            client2 = second;
            clientcount = 2;
            client1.Move += Client_Move;
            client1.Turn += Client_Turn;
            client1.GameOver += Client_GameOver;
            client1.Draw += Client_Draw;
            client1.Error += Client_Error;
            client1.Upgrade += Client_Upgrade;
            client1.UpgradeDia += Client_UpgradeDia;
            client1.SocketError += Client_SocketError;
            client1.OwnGet += Client_OwnGet;

            client2.Move += Client_Move;
            client2.Turn += Client_Turn;
            client2.GameOver += Client_GameOver;
            client2.Draw += Client_Draw;
            client2.Error += Client_Error;
            client2.Upgrade += Client_Upgrade;
            client2.UpgradeDia += Client_UpgradeDia;
            client2.SocketError += Client_SocketError;
            client2.OwnGet += Client_OwnGet;

            client1.SendOk();
            client2.SendOk();
        }

        private void Client_UpgradeDia(ChessClient sender)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                UpgradeDialog dia = new UpgradeDialog();
                dia.Owner = Parent;
                dia.Who = sender.Own;
                dia.ShowDialog();
                sender.SendUpgrade(dia.Type);
            }));
        }

        private void Client_Upgrade(ChessClient sender, UpgradeType type)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                p.Upgrade(type);
                ReDraw();
                CheckPaint();
            }));
        }

        private void Client_Error(ChessClient sender)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                YesNoDialog yn = new YesNoDialog();
                yn.Owner = Parent;
                yn.CaptionText = "錯誤";
                yn.ContentText = "發生預料外的錯誤";
                yn.OKOnly = true;
                yn.ShowDialog();
            }));
        }

        private void Client_Draw(ChessClient sender, bool accept)
        {
            if (accept)
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    YesNoDialog yn = new YesNoDialog();
                    yn.Owner = Parent;
                    yn.CaptionText = "協議和局";
                    yn.ContentText = "對方提出了和局的要求，你是否接受？";
                    yn.ShowDialog();
                    sender.SendDraw(yn.ReturnValue);
                }));
            }
            else
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    YesNoDialog yn = new YesNoDialog();
                    yn.Owner = Parent;
                    yn.CaptionText = "協議和局失敗";
                    yn.ContentText = "對方拒絕了你的要求";
                    yn.OKOnly = true;
                    yn.ShowDialog();
                }));
            }
        }

        private void Client_GameOver(ChessClient sender, GameOverType type)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (clientcount == 2)
                {
                    if (sender.Own == ChessOwner.Black) return;
                    switch (type)
                    {
                        case GameOverType.WhiteWin:
                        case GameOverType.BlackWin:
                        case GameOverType.BlackSurrend:
                        case GameOverType.WhiteSurrend:
                        case GameOverType.Disconnect:
                            ct = CaptionType.Victory;
                            break;
                        case GameOverType.Draw:
                            ct = CaptionType.Draw;
                            break;
                    }
                    godc = type;
                    Thread th = new Thread(new ThreadStart(GameOver));
                    th.Start();
                }
                else if (clientcount == 1)
                {
                    ChessOwner me = sender.Own;
                    switch (type)
                    {
                        case GameOverType.WhiteWin:
                            ct = (me == ChessOwner.White ? CaptionType.Victory : CaptionType.Defeat);
                            break;
                        case GameOverType.BlackWin:
                            ct = (me != ChessOwner.White ? CaptionType.Victory : CaptionType.Defeat);
                            break;
                        case GameOverType.BlackSurrend:
                            ct = (me == ChessOwner.White ? CaptionType.Victory : CaptionType.Defeat);
                            break;
                        case GameOverType.WhiteSurrend:
                            ct = (me != ChessOwner.White ? CaptionType.Victory : CaptionType.Defeat);
                            break;
                        case GameOverType.Draw:
                            ct = CaptionType.Draw;
                            break;
                        case GameOverType.Disconnect:
                            ct = CaptionType.Victory;
                            break;
                    }
                    godc = type;
                    Thread th = new Thread(new ThreadStart(GameOver));
                    th.Start();
                }
            }));
            if (client1 != null)
            {
                client1.Dispose();
            }
            if (client2 != null)
            {
                client2.Dispose();
            }
        }

        private CaptionType ct;
        private GameOverType godc;
        private void GameOver()
        {
            Dispatcher.BeginInvoke(new Action(godshow));            
        }
        //private bool Isgod = false;
        private void godshow()
        {
            //if (Isgod) return;
            Thread.Sleep(1000);
            GameOverDialog god = new GameOverDialog();
            god.Owner = Parent;
            god.Caption = ct;
            god.Property = godc;
            //Isgod = true;
            god.ShowDialog();
            //Isgod = false;
        }
        private void Client_Turn(ChessClient sender)
        {
            moving = sender.Own;
        }

        private void Client_Move(ChessClient sender, int fx, int fy, int tx, int ty)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                b[p.LastMove.X, p.LastMove.Y].LastMoveBorder = false;
                p.Move(new XY(fx, fy), new XY(tx, ty));
                ReDraw();
                b[p.LastMove.X, p.LastMove.Y].LastMoveBorder = true;
                CheckPaint();
                if(Parent.WindowState == WindowState.Minimized)
                {
                    Parent.Flash();
                }              
            }));
        }
        private void CheckPaint()
        {
            foreach (var item in b)
            {
                if (item.ShowWarn) item.ShowWarn = false;
            }
            bool result;
            XY king;
            result = p.CheckCheck(ChessOwner.Black, out king);
            if (king.X != -1) b[king.X, king.Y].ShowWarn = result;
            result = p.CheckCheck(ChessOwner.White, out king);
            if (king.X != -1) b[king.X, king.Y].ShowWarn = result;
        }


        private void Fliper()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (!flip)
                    {
                        b[i, j].Margin = new Thickness(j * 64, i * 64, 0, 0);
                    }
                    else
                    {
                        b[i, j].Margin = new Thickness((7 - j) * 64, (7 - i) * 64, 0, 0);
                    }
                }
            }
        }
        private void Init()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Block tmp = new Block(i, j);
                    if ((i + j) % 2 == 0) tmp.BackColor = Colors.WhiteSmoke;
                    else tmp.BackColor = Colors.DimGray;
                    tmp.Chess = p[i, j].Type;
                    tmp.HorizontalAlignment = HorizontalAlignment.Left;
                    tmp.VerticalAlignment = VerticalAlignment.Top;
                    tmp.Width = 64;
                    tmp.Height = 64;
                    tmp.Opacity = 0.75;
                    if (!flip)
                    {
                        tmp.Margin = new Thickness(j * 64, i * 64, 0, 0);
                    }
                    else
                    {
                        tmp.Margin = new Thickness((7 - j) * 64, (7 - i) * 64, 0, 0);
                    }
                    MainGrid.Children.Add(tmp);
                    tmp.MouseDown += Blocks_MouseDown;
                    b[i, j] = tmp;
                }
            }
        }
        private void ReDraw()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    b[i, j].Chess = p[i, j].Type;
                }
            }
        }
        private void ClearSelect()
        {
            selectx = -1;
            selecty = -1;
            foreach (var item in b)
            {
                item.Hide();
            }
        }
        private void Blocks_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Block source = (Block)sender;
            int x = source.X;
            int y = source.Y;
            if (selectx != -1 && b[source.X, source.Y].Selected && !(selectx == source.X && selecty == source.Y))
            {
                ChessOwner target = moving;
                moving = ChessOwner.None;
                if (target != ChessOwner.None) (target == client1.Own ? client1 : client2).SendMove(new XY(selectx, selecty), new XY(x, y));
                ClearSelect();
                return;
            }
            if (moving != p[x, y].Owner) return;
            if (x == selectx && y == selecty)
            {
                ClearSelect();
                return;
            }
            ClearSelect();
            if (selectx != -1)
            {
                b[selectx, selecty].Hide(true);
            }
            if (p[x, y].Type == ChessType.None) return;
            selectx = x;
            selecty = y;
            source.Show(Colors.Goldenrod);
            List<XY> moves = p.SafeMoveSearch(new XY(x, y));
            foreach (var item in moves)
            {
                switch (p.MoveDetect(new XY(x, y), item))
                {
                    case MoveType.Move:
                        b[item.X, item.Y].Show(Colors.SkyBlue);
                        break;
                    case MoveType.Attack:
                        b[item.X, item.Y].Show(Colors.Firebrick);
                        break;
                    case MoveType.Upgrade:
                        b[item.X, item.Y].Show(Colors.BlueViolet);
                        break;
                    case MoveType.En_Passant:
                        b[item.X, item.Y].Show(Colors.Firebrick);
                        break;
                    case MoveType.Castling:
                        b[item.X, item.Y].Show(Colors.SeaGreen);
                        break;
                }
            }
        }

        private void MenuBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ChessMenu cm = new ChessMenu();
            cm.Owner = Parent;
            cm.ShowDialog();
            switch (cm.Type)
            {
                case MenuReturnType.Draw:
                    if (moving != ChessOwner.None) (moving == client1.Own ? client1 : client2).SendDraw(true);
                    break;
                case MenuReturnType.Surrend:
                    if (moving != ChessOwner.None) (moving == client1.Own ? client1 : client2).SendSurrend();
                    break;
            }
        }
    }
}
