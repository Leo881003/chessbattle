﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using ChessCore;

namespace ChessBattle
{
    /// <summary>
    /// Interaction logic for SingleTest.xaml
    /// </summary>
    public partial class SingleTest : Window
    {
        private Block[,] b = new Block[8, 8];
        private ChessPanel p = new ChessPanel();
        private int selectx = -1;
        private int selecty = -1;
        public bool Isfilp
        {
            get;
            set;
        }
        public SingleTest()
        {
            InitializeComponent();
            Init();
        }
        private void Init()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Block tmp = new Block(i, j);
                    if ((i + j) % 2 == 0) tmp.BackColor = Colors.WhiteSmoke;
                    else tmp.BackColor = Colors.DimGray;
                    //if (!Isfilp) tmp.Chess = p[7 - i, 7 - j].Type;
                    //else tmp.Chess = p[i, j].Type;
                    tmp.Chess = p[i, j].Type;
                    tmp.HorizontalAlignment = HorizontalAlignment.Left;
                    tmp.VerticalAlignment = VerticalAlignment.Top;
                    tmp.Width = 64;
                    tmp.Height = 64;
                    tmp.Margin = new Thickness(j * 64, i * 64, 0, 0);
                    MainGrid.Children.Add(tmp);
                    tmp.MouseDown += Blocks_MouseDown;
                    b[i, j] = tmp;
                }
            }
        }
        private void ClearSelect()
        {
            selectx = -1;
            selecty = -1;
            foreach (var item in b)
            {
                item.Hide();
            }
        }
        private void Blocks_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Block source = (Block)sender;
            if (selectx != -1 && b[source.X, source.Y].Selected && !(selectx == source.X && selecty == source.Y))
            {
                p.Move(new XY(selectx, selecty), new XY(source.X, source.Y));
                b[source.X, source.Y].Chess = b[selectx, selecty].Chess;
                b[selectx, selecty].Chess = ChessType.None;
                ClearSelect();
                return;
            }
            ClearSelect();
            int x = source.X;
            int y = source.Y;
            if (selectx != -1)
            {
                b[selectx, selecty].Hide(true);
            }
            if (p[x, y].Type == ChessType.None) return;
            selectx = x;
            selecty = y;
            source.Show(Colors.Wheat);
            List<XY> moves = p.SafeMoveSearch(new XY(x, y));
            foreach (var item in moves)
            {
                //if (Isfilp) b[7 - item.X, 7 - item.Y].Show(Colors.SkyBlue);
                //else b[item.X, item.Y].Show(Colors.SkyBlue);
                b[item.X, item.Y].Show(Colors.SkyBlue);
            }
        }
    }
}