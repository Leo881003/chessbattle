﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NetCore;

namespace ChessBattle
{
    /// <summary>
    /// MainMenuPage.xaml 的互動邏輯
    /// </summary>
    public partial class MainMenuPage : Page
    {
        public new MainWindow Parent;
        public MainMenuPage()
        {
            InitializeComponent();
        }

        private void SingleBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Parent.Single();
        }

        private void ExitBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Environment.Exit(0);
        }

        private void NetBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Parent.Server();
        }

        private void ClientBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Parent.Client();
        }
    }
}
