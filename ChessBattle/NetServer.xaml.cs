﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NetCore;
using ChessCore;

namespace ChessBattle
{
    /// <summary>
    /// NetServer.xaml 的互動邏輯
    /// </summary>
    public partial class NetServer : Page
    {
        ChessServer cs;
        ChessClient cc;
        LocalTransfer lt;
        NetTransfer nt;
        ChessPage cp;
        public new MainWindow Parent;
        public NetServer()
        {
            InitializeComponent();
            ServerStart();
        }
        private void ServerStart()
        {
            try
            {
                nt = new NetTransfer();
                nt.Accepted += Connected;
                lt = new LocalTransfer();
                cc = new ChessClient(lt, null);
                cs = new ChessServer(nt, lt);
                cp = new ChessPage(cc);                
                IPList.Content = GetLocalIPAddress();
            }
            catch (SocketException ex)
            {
                YesNoDialog yn = new YesNoDialog();
                yn.CaptionText = "連線錯誤";
                yn.ContentText = ex.Message;
                yn.Owner = Parent;
                yn.OKOnly = true;
                yn.ShowDialog();
            }
        }
        private void Connected()
        {
            Dispatcher.Invoke(new Action(() =>
            {
                cp.Parent = Parent;
                Parent.SwitchToChess(cp);
            }));
        }
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            string output = "";
            foreach (var ip in host.AddressList)
            {
                System.Diagnostics.Debug.Print(ip.ToString());
            }
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    output += ip.ToString() + "\n";
                }
            }
            return output;
        }

        private void BackBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            cs.Dispose();
            cc.Dispose();
            Parent.MainMenu();
        }
    }
}
