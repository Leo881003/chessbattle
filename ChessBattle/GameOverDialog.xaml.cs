﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ChessCore;
using NetCore;

namespace ChessBattle
{
    /// <summary>
    /// GameOverDialog.xaml 的互動邏輯
    /// </summary>
    public partial class GameOverDialog : Window
    {
        private GameOverType godc;
        private CaptionType capty = CaptionType.Victory;
        public GameOverDialog()
        {
            InitializeComponent();
        }
        public CaptionType Caption
        {
            get
            {
                return capty;
            }
            set
            {
                capty = value;
                switch (capty)
                {
                    case CaptionType.Victory:
                        VicText.Visibility = Visibility.Visible;
                        DefeatText.Visibility = Visibility.Hidden;
                        DrawText.Visibility = Visibility.Hidden;
                        break;
                    case CaptionType.Defeat:
                        VicText.Visibility = Visibility.Hidden;
                        DefeatText.Visibility = Visibility.Visible;
                        DrawText.Visibility = Visibility.Hidden;
                        break;
                    case CaptionType.Draw:
                        VicText.Visibility = Visibility.Hidden;
                        DefeatText.Visibility = Visibility.Hidden;
                        DrawText.Visibility = Visibility.Visible;
                        break;
                }
            }
        }
        public GameOverType Property
        {
            get
            {
                return godc;
            }
            set
            {
                godc = value;
                switch (godc)
                {
                    case GameOverType.WhiteWin:
                        Text.Content = "白方獲得了本局的勝利";
                        break;
                    case GameOverType.BlackWin:
                        Text.Content = "黑方獲得了本局的勝利";
                        break;
                    case GameOverType.WhiteSurrend:
                        Text.Content = "白方已經投降";
                        break;
                    case GameOverType.BlackSurrend:
                        Text.Content = "黑方已經投降";
                        break;
                    case GameOverType.Draw:
                        Text.Content = "本局和局";
                        break;
                    case GameOverType.Disconnect:
                        Text.Content = "對方斷線";
                        break;
                }
            }
        }

        private void MainMenuBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ((MainWindow)Owner).MainMenu();
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ((MainWindow)Owner).MainMenu();
        }
    }

    public enum CaptionType
    {
        Victory,
        Defeat,
        Draw
    }
}
