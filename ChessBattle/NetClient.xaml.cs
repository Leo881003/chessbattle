﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NetCore;
using ChessCore;

namespace ChessBattle
{
    /// <summary>
    /// NetClient.xaml 的互動邏輯
    /// </summary>
    public partial class NetClient : Page
    {
        public new MainWindow Parent;
        ClientTransfer ct;
        ChessClient cc;
        ChessPage cp;
        public NetClient()
        {
            InitializeComponent();
        }

        private void ClientStart()
        {
            try
            {
                ct = new ClientTransfer();
                ct.Accepted += AC;
                IPAddress ip = IPAddress.Parse(IPTextBox.Text);
                if(ip.AddressFamily == AddressFamily.InterNetworkV6)
                {
                    YesNoDialog yn = new YesNoDialog();
                    yn.CaptionText = "目前不支援IPv6";
                    yn.ContentText = "";
                    yn.Owner = Parent;
                    yn.OKOnly = true;
                    yn.ShowDialog();
                    return;
                }
                IPEndPoint ep = new IPEndPoint(ip, 7500);
                cc = new ChessClient(ct, ep);
                cp = new ChessPage(cc);               
            }
            catch (SocketException ex)
            {
                YesNoDialog yn = new YesNoDialog();
                yn.CaptionText = "連線錯誤";
                yn.ContentText = ex.Message;
                yn.Owner = Parent;
                yn.OKOnly = true;
                yn.ShowDialog();
            }
            catch (FormatException)
            {
                YesNoDialog yn = new YesNoDialog();
                yn.CaptionText = "請輸入正確的IP位址";
                yn.ContentText = "";
                yn.Owner = Parent;
                yn.OKOnly = true;
                yn.ShowDialog();
            }
        }
        private void AC()
        {
            Dispatcher.Invoke(new Action(() =>
            {
                cp.Parent = Parent;
                Parent.SwitchToChess(cp);
            }));
        }
        private void BackBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Parent.MainMenu();
        }

        private void OKBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ClientStart();
        }
    }
}
