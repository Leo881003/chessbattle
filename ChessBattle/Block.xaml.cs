﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ChessCore;

namespace ChessBattle
{
    /// <summary>
    /// Block.xaml 的互動邏輯
    /// </summary>
    public partial class Block : UserControl
    {
        private static ImageSource[] ChessSource = new ImageSource[13];
        private static bool SourceInited = false;
        private ChessType chessty = ChessType.None;
        Storyboard sb;
        private static void SourceInit()
        {
            if (SourceInited) return;
            ChessSource[0] = null;
            ChessSource[1] = new BitmapImage(new Uri("Chess/Chess-3/Chess_kdt45.png", UriKind.Relative));
            ChessSource[2] = new BitmapImage(new Uri("Chess/Chess-3/Chess_qdt45.png", UriKind.Relative));
            ChessSource[3] = new BitmapImage(new Uri("Chess/Chess-3/Chess_bdt45.png", UriKind.Relative));
            ChessSource[4] = new BitmapImage(new Uri("Chess/Chess-3/Chess_ndt45.png", UriKind.Relative));
            ChessSource[5] = new BitmapImage(new Uri("Chess/Chess-3/Chess_rdt45.png", UriKind.Relative));
            ChessSource[6] = new BitmapImage(new Uri("Chess/Chess-3/Chess_pdt45.png", UriKind.Relative));
            ChessSource[7] = new BitmapImage(new Uri("Chess/Chess-3/Chess_klt45.png", UriKind.Relative));
            ChessSource[8] = new BitmapImage(new Uri("Chess/Chess-3/Chess_qlt45.png", UriKind.Relative));
            ChessSource[9] = new BitmapImage(new Uri("Chess/Chess-3/Chess_blt45.png", UriKind.Relative));
            ChessSource[10] = new BitmapImage(new Uri("Chess/Chess-3/Chess_nlt45.png", UriKind.Relative));
            ChessSource[11] = new BitmapImage(new Uri("Chess/Chess-3/Chess_rlt45.png", UriKind.Relative));
            ChessSource[12] = new BitmapImage(new Uri("Chess/Chess-3/Chess_plt45.png", UriKind.Relative));
            SourceInited = true;
        }
        public Block()
        {
            InitializeComponent();
            if (!SourceInited) SourceInit();
            Selected = false;
            GoldenBorder = true;
            LastMoveBorder = false;
            ShowWarn = false;
        }
        public Block(int x,int y):this()
        {
            X = x;
            Y = y;
        }
        public int X
        {
            get;
            private set;
        }
        public int Y
        {
            get;
            private set;
        }
        public bool GoldenBorder
        {
            get;
            set;
        }
        public bool LastMoveBorder
        {
            get
            {
                return Lastmv.Visibility == Visibility.Visible;
            }
            set
            {
                Lastmv.Visibility = (value ? Visibility.Visible : Visibility.Hidden);
            }
        }
        public bool MouseMovingBlur
        {
            get;
            set;
        }
        public bool ShowWarn
        {
            get
            {
                return Warning.Visibility == Visibility.Visible;
            }
            set
            {
                Warning.Visibility = (value ? Visibility.Visible : Visibility.Hidden);
            }
        }
        public Color SelectBorder
        {
            get
            {
                return ((GradientBrush)Select.Stroke).GradientStops[0].Color;
            }
            set
            {
                value.A = 255;
                Color light = new Color();
                light.A = 255;
                light.R = Per(value.R, 0.6);
                light.G = Per(value.G, 0.6);
                light.B = Per(value.B, 0.6);
                ((GradientBrush)Select.Stroke).GradientStops[1].Color = value;
                ((GradientBrush)Select.Stroke).GradientStops[1].Color = light;
            }
        }
        public Color SelectFill
        {
            get
            {
                return ((GradientBrush)Select.Fill).GradientStops[1].Color;
            }
            set
            {
                value.A = 178;
                Color light = new Color();
                light.A = 255;
                light.R = Per(value.R, 0.3);
                light.G = Per(value.G, 0.3);
                light.B = Per(value.B, 0.3);
                ((GradientBrush)Select.Fill).GradientStops[0].Color = light;
                ((GradientBrush)Select.Fill).GradientStops[1].Color = value;
            }
        }
        private static byte Per(byte val,double times)
        {
            return (byte)(val * times + 255.0 * (1.0 - times));
        }

        public Color BackColor
        {
            get
            {
                return ((SolidColorBrush)Back.Fill).Color;
            }
            set
            {
                ((SolidColorBrush)Back.Fill).Color = value;
            }
        }
        public ChessType Chess
        {
            get
            {
                return chessty;
            }
            set
            {
                chessty = value;
                ChessImg.Source = ChessSource[(byte)chessty];
            }
        }
        private void UserControl_MouseEnter(object sender, MouseEventArgs e)
        {
            if(GoldenBorder)
                BorderImg.Visibility = Visibility.Visible;
            if (MouseMovingBlur)
                Show();
        }

        private void UserControl_MouseLeave(object sender, MouseEventArgs e)
        {
            BorderImg.Visibility = Visibility.Hidden;
            if (MouseMovingBlur)
                Hide();
        }

        public void Show()
        {
            if (sb != null) sb.Stop();
            sb = FindResource("SelectShow") as Storyboard;
            sb.Begin();
            Selected = true;
        }
        public void Show(Color val)
        {
        	SelectFill = val;
        	Show();
        }
        public void Hide(bool animat = false)
        {
            if (sb != null) sb.Stop();
            if (animat)
            {
                sb = FindResource("SelectHidden") as Storyboard;
                sb.Begin();
                Selected = false;
            }
            else
            {
                Select.Visibility = Visibility.Hidden;
                Select.Opacity = 0;
                Selected = false;
            }
        }
        public bool Selected
        {
            get;
            private set;
        }
    }
}
