﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ChessBattle
{
    /// <summary>
    /// YesNoDialog.xaml 的互動邏輯
    /// </summary>
    public partial class YesNoDialog : Window
    {

        public YesNoDialog()
        {
            InitializeComponent();
        }
        private bool okonly = false;
        public bool OKOnly
        {
            get
            {
                return okonly;
            }
            set
            {
                if (okonly == value) return;
                okonly = value;
                if (okonly)
                {
                    YesBtn.Visibility = Visibility.Hidden;
                    NoBtn.Visibility = Visibility.Hidden;
                    OKBtn.Visibility = Visibility.Visible;
                }
                else
                {
                    YesBtn.Visibility = Visibility.Visible;
                    NoBtn.Visibility = Visibility.Visible;
                    OKBtn.Visibility = Visibility.Hidden;
                }
            }
        }

        public bool ReturnValue
        {
            get;
            private set;
        }

        public string CaptionText
        {
            get
            {
                return (string)Caption.Content;
            }
            set
            {
                Caption.Content = value;
            }
        }

        public string ContentText
        {
            get
            {
                return (string)Text.Content;
            }
            set
            {
                Text.Content = value;
            }
        }

        private void YesBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ReturnValue = true;
            Close();
        }

        private void NoBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ReturnValue = false;
            Close();
        }

        private void OKBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Close();
        }
    }
}
