﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ChessCore;
using NetCore;
using System.Windows.Interop;
using System.Runtime.InteropServices;
using System.Threading;

namespace ChessBattle
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        public static MainWindow mother;
        MainMenuPage mmp;
        ChessPage cp;
        NetServer ns;
        NetClient nc;
        public MainWindow()
        {
            InitializeComponent();
            MainMenu();
            mother = this;
            DebugControl.StartDebugWindows();
        }
        public void MainMenu()
        {
            mmp = new MainMenuPage();
            mmp.Parent = this;
            Content = mmp;
        }
        public void Single()
        {
            LocalTransfer fl = new LocalTransfer();
            LocalTransfer sl = new LocalTransfer();
            ChessClient fc = new ChessClient(fl, null);
            ChessClient sc = new ChessClient(sl, null);
            ChessServer server = new ChessServer(sl, fl);
            cp = new ChessPage(fc, sc);
            cp.Parent = this;
            Content = cp;
        }
        public void Server()
        {
            ns = new NetServer();
            ns.Parent = this;
            Content = ns;
        }
        public void Client()
        {
            nc = new NetClient();
            nc.Parent = this;
            Content = nc;
        }
        public void SwitchToChess(ChessPage cp)
        {
            Content = cp;
        }

        private int _WINDOWHANDLE;
        private bool _FLASHINGTASKBAR;
        private bool _ISACTIVE;
        [DllImport("user32")]
        public static extern int FlashWindow(int hwnd, int bInvert);

        public void Flash()
        {
            _FLASHINGTASKBAR = true;
            _ISACTIVE = false;
            _WINDOWHANDLE = (int)(new WindowInteropHelper(this)).Handle;
            FlashWindow((int)(new WindowInteropHelper(this)).Handle, 1);
            Thread td = new Thread(new ThreadStart(FlashTaskBar));
            td.Start();
        }

        private void FlashTaskBar()
        {
            for (int i = 0; i < 30; i++)
            {
                if (_ISACTIVE)
                {
                    break;
                }
                else
                {
                    System.Threading.Thread.Sleep(1000);
                    FlashWindow(_WINDOWHANDLE, 1);
                }
            }
            _FLASHINGTASKBAR = false;
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            _ISACTIVE = true;
        }
    }
}
