﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ChessBattle
{
    /// <summary>
    /// Test.xaml 的互動邏輯
    /// </summary>
    public partial class Test : Window
    {
        public Test()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            tar.Show();
        }

        private void sliderR_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Color c = new Color();
            c.R = (byte)sliderR.Value;
            c.G = (byte)sliderG.Value;
            c.B = (byte)sliderB.Value;
            tar.SelectFill = c;
        }

        private void tar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            tar.Show();
        }
    }
}
