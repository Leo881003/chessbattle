﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ChessBattle
{
    
    public partial class BorderButton : UserControl
    {
        private Color RawColor = Colors.DodgerBlue;
        private Color LightColor;
        private Color DeepColor;
        private const byte LRDColor = 50;
        public BorderButton()
        {
            InitializeComponent();
            ((BlurEffect)Border.Effect).Radius = 0;
        }

        private void UserControl_MouseEnter(object sender, MouseEventArgs e)
        {
            SetBorderColor(LightColor);
            ((BlurEffect)Border.Effect).Radius = 3;
        }

        private void UserControl_MouseLeave(object sender, MouseEventArgs e)
        {
            SetBorderColor(RawColor);
            ((BlurEffect)Border.Effect).Radius = 0;
        }

        private void UserControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            SetBorderColor(DeepColor);
        }

        private void UserControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            SetBorderColor(RawColor);
        }
        private Color Deep()
        {
            Color cl = new Color();
            cl.A = 255;
            cl.R = FixByte(RawColor.R - LRDColor);
            cl.G = FixByte(RawColor.G - LRDColor);
            cl.B = FixByte(RawColor.B - LRDColor);           
            return cl;
        }

        private Color Light()
        {
            Color cl = new Color();
            cl.A = 255;
            cl.R = FixByte(RawColor.R + LRDColor);
            cl.G = FixByte(RawColor.G + LRDColor);
            cl.B = FixByte(RawColor.B + LRDColor);
            return cl;
        }
        private byte FixByte(int n)
        {
            if (n > byte.MaxValue) return byte.MaxValue;
            if (n < byte.MinValue) return byte.MinValue;
            return Convert.ToByte(n);
        }
        public Color BorderColor
        {
            get
            {
                return ((SolidColorBrush)Border.Stroke).Color;
            }
            set
            {
                SetBorderColor(value);
                RawColor = value;
                LightColor = Light();
                DeepColor = Deep();
            }
        }
        private void SetBorderColor(Color cl)
        {
            ((SolidColorBrush)Border.Stroke).Color = cl;
        }
        public double BorderWidth
        {
            get
            {
                return Border.StrokeThickness;
            }
            set
            {
                Border.StrokeThickness = value;
            }
        }
        public Brush BackColor
        {
            get
            {
                return Border.Fill;
            }
            set
            {
                Border.Fill = value;
            }
        }
        public string LabelText
        {
            get
            {
                return (string)Text.Content;
            }
            set
            {
                Text.Content = value;
            }
        }
        public double LabelSize
        {
            get
            {
                return Text.FontSize;
            }
            set
            {
                Text.FontSize = value;
            }
        }
        public Brush LabelColor
        {
            get
            {
                return Text.Foreground;
            }
            set
            {
                Text.Foreground = value;
            }
        }
    }
}
