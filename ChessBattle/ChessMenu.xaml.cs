﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ChessBattle
{
    /// <summary>
    /// ChessMenu.xaml 的互動邏輯
    /// </summary>
    public partial class ChessMenu : Window
    {
        public ChessMenu()
        {
            InitializeComponent();
        }

        public MenuReturnType Type
        {
            get;
            private set;
        }

        private void BackBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Type = MenuReturnType.None;
            Close();
        }

        private void SurrendBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            YesNoDialog yn = new YesNoDialog();
            yn.Owner = Owner;
            yn.CaptionText = "投降";
            yn.ContentText = "你確定要投降嗎？";
            yn.ShowDialog();
            if (!yn.ReturnValue) return;
            Type = MenuReturnType.Surrend;
            Close();
        }

        private void DrawBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            YesNoDialog yn = new YesNoDialog();
            yn.Owner = Owner;
            yn.CaptionText = "協議和局";
            yn.ContentText = "你確定要提出和局嗎？";
            yn.ShowDialog();
            if (!yn.ReturnValue) return;
            Type = MenuReturnType.Draw;
            Close();
        }
    }
    public enum MenuReturnType
    {
        None,
        Draw,
        Surrend
    }
}
