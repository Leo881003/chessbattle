﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessCore
{
    public class ChessBlock : ICloneable
    {
        public readonly int id;
        public readonly int x;
        public readonly int y;
        private ChessType ty;
        public ChessOwner Owner
        {
            get;
            private set;
        }
        public bool Moved
        {
            get;
            private set;
        }
        public bool BlackControl
        {
            get;
            set;
        }
        public bool WhiteControl
        {
            get;
            set;
        }
        public void SetDefault(ChessType type, bool moved = false)
        {
            ty = type;
            Moved = moved;
            if (type == ChessType.None)
                Owner = ChessOwner.None;
            else if ((int)type >= 1 && (int)type <= 6)
                Owner = ChessOwner.Black;
            else Owner = ChessOwner.White;
        }

        public object Clone()
        {
            return new ChessBlock(id, Type, Moved);
        }

        public ChessType Type
        {
            get
            {
                return ty;
            }
            set
            {
                if (ty == value)
                    return;
                ty = value;
                Moved = true;
                if (value == ChessType.None)
                    Owner = ChessOwner.None;
                else if ((int)value >= 1 && (int)value <= 6)
                    Owner = ChessOwner.Black;
                else Owner = ChessOwner.White;
            }
        }
        public ChessBlock(int id)
        {
            BlackControl = false;
            WhiteControl = false;
            Type = ChessType.None;
            Owner = ChessOwner.None;
            Moved = true;
            this.id = id;
            x = 7 - (id / 8);
            y = id % 8;
        }
        private ChessBlock(int id, ChessType type, bool moved) : this(id)
        {
            Type = type;
            Moved = moved;
        }
    }
}
