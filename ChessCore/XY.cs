﻿using System;

namespace ChessCore
{
	public struct XY
	{
		public int X
		{
			get;
			set;
		}
		public int Y
		{
			get;
			set;
		}
		public XY(int x,int y):this()
		{
			X = x;
			Y = y;
		}
        public static bool operator ==(XY a,XY b)
        {
            if (a.X == b.X && a.Y == b.Y) return true;
            else return false;
        }
        public static bool operator !=(XY a, XY b)
        {
            if (a == b) return false;
            else return true;
        }
        public override bool Equals(object obj)
        {
            if (this == (XY)obj) return true;
            else return false;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
