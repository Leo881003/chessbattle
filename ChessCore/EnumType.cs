﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessCore
{
    public enum ChessType : byte
    {
        None = 0,
        BlackKing,
        BlackQueen,
        BlackBishop,
        BlackKnight,
        BlackRook,
        BlackPawn,
        WhiteKing,
        WhiteQueen,
        WhiteBishop,
        WhiteKnight,
        WhiteRook,
        WhitePawn
    }
    public enum ChessOwner : byte
    {
        None = 0,
        White = 1,
        Black = 2
    }
    public enum MoveType
    {
        Move,
        Blocked,
        Attack,
        Upgrade,
        En_Passant,
        Castling,
        OutOfPanel = 99
    }
    public enum UpgradeType : byte
    {
        Queen = 4,
        Bishop = 3,
        Knight = 2,
        Rook = 1
    }
    public enum CheckMateType : byte
    {
        None = 0,
        CheckMate = 1,
        Draw = 2
    }
}
