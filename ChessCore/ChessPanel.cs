using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChessCore
{
    public class ChessPanel : IEnumerable
    {
        private ChessBlock[,] pl = new ChessBlock[8, 8];
        public ChessOwner Upgrading
        {
            get;
            private set;
        }
        public ChessPanel()
        {
            int o = 0;
            for (int x = 7; x >= 0; x--)
            {
                for (int y = 0; y < 8; y++)
                {
                    pl[x, y] = new ChessBlock(o);
                    o++;
                }
            }
            #region Init Chesses
            for (int i = 0; i < 8; i++)
            {
                pl[1, i].SetDefault(ChessType.WhitePawn);
            }
            for (int i = 0; i < 8; i++)
            {
                pl[6, i].SetDefault(ChessType.BlackPawn);
            }
            pl[0, 0].SetDefault(ChessType.WhiteRook);
            pl[0, 7].SetDefault(ChessType.WhiteRook);
            pl[0, 1].SetDefault(ChessType.WhiteKnight);
            pl[0, 6].SetDefault(ChessType.WhiteKnight);
            pl[0, 2].SetDefault(ChessType.WhiteBishop);
            pl[0, 5].SetDefault(ChessType.WhiteBishop);
            pl[0, 4].SetDefault(ChessType.WhiteQueen);
            pl[0, 3].SetDefault(ChessType.WhiteKing);

            pl[7, 0].SetDefault(ChessType.BlackRook);
            pl[7, 7].SetDefault(ChessType.BlackRook);
            pl[7, 1].SetDefault(ChessType.BlackKnight);
            pl[7, 6].SetDefault(ChessType.BlackKnight);
            pl[7, 2].SetDefault(ChessType.BlackBishop);
            pl[7, 5].SetDefault(ChessType.BlackBishop);
            pl[7, 4].SetDefault(ChessType.BlackQueen);
            pl[7, 3].SetDefault(ChessType.BlackKing);
            #endregion
            Upgrading = ChessOwner.None;
        }
        public XY LastMove
        {
            get;
            private set;
        }
        public XY LastMoveTo
        {
            get;
            private set;
        }
        public ChessType LastChess
        {
            get;
            private set;
        }
        public byte[] GetBytes()
        {
            List<byte> ls = new List<byte>();
            for (int x = 7; x >= 0; x--)
            {
                for (int y = 0; y < 8; y++)
                {
                    ls.Add((byte)pl[x, y].Type);
                    ls.AddRange(BitConverter.GetBytes(pl[x, y].Moved));
                }
            }
            return ls.ToArray();
        }
        public bool CheckCheck(ChessOwner owner, out XY position, bool Recalc = true)
        {
            position = new XY(-1, -1);
            if (owner == ChessOwner.None) return false;
            if (Recalc) RecalcSafe();
            foreach (ChessBlock item in pl)
            {
                if (IsKing(item.Type) && item.Owner == owner)
                {
                    position = new XY(item.x, item.y);
                    return (owner == ChessOwner.White ? item.BlackControl : item.WhiteControl);
                }
            }
            return false;
        }
        public CheckMateType CheckMate(ChessOwner owner)
        {
            RecalcSafe();
            XY tmp;
            if (StepsCanChoose(owner, false) > 0) return CheckMateType.None;
            else
            {
                if (CheckCheck(owner, out tmp, false)) return CheckMateType.CheckMate;
                else return CheckMateType.Draw;
            }
        }
        public int StepsCanChoose(ChessOwner owner, bool Recalc = true)
        {
            if (owner == ChessOwner.None) return 0;
            if (Recalc) RecalcSafe();
            int c = 0;
            foreach (ChessBlock item in pl)
            {
                if (item.Owner == owner)
                {
                    c += SafeMoveSearch(new XY(item.x, item.y)).Count;
                }
            }
            return c;
        }
        public List<XY> CanCastling(XY t)
        {
            int x = t.X;
            int y = t.Y;
            List<XY> ls = new List<XY>();
            #region WhiteKing
            if (pl[x, y].Type == ChessType.WhiteKing)
            {
                RecalcSafe();
                //Short
                if (!(pl[x, y].Moved || pl[x, 0].Moved) && pl[x, 0].Type == ChessType.WhiteRook)
                {
                    if (pl[x, y - 1].Type == ChessType.None && pl[x, y - 2].Type == ChessType.None)
                    {
                        if (!(pl[x, y].BlackControl || pl[x, y - 1].BlackControl || pl[x, y - 2].BlackControl))
                        {
                            ls.Add(new XY(x, y - 2));
                        }
                    }
                }
                //Long
                if (!(pl[x, y].Moved || pl[x, 7].Moved) && pl[x, 7].Type == ChessType.WhiteRook)
                {
                    if (pl[x, y + 1].Type == ChessType.None && pl[x, y + 2].Type == ChessType.None)
                    {
                        if (!(pl[x, y].BlackControl || pl[x, y + 1].BlackControl || pl[x, y + 2].BlackControl))
                        {
                            ls.Add(new XY(x, y + 2));
                        }
                    }
                }
            }
            #endregion
            #region BlackKing
            else if (pl[x, y].Type == ChessType.BlackKing)
            {
                RecalcSafe();
                //Short
                if (!(pl[x, y].Moved || pl[x, 0].Moved) && pl[x, 0].Type == ChessType.BlackRook)
                {
                    if (pl[x, y - 1].Type == ChessType.None && pl[x, y - 2].Type == ChessType.None)
                    {
                        if (!(pl[x, y].WhiteControl || pl[x, y - 1].WhiteControl || pl[x, y - 2].WhiteControl))
                        {
                            ls.Add(new XY(x, y - 2));
                        }
                    }
                }
                //Long
                if (!(pl[x, y].Moved || pl[x, 7].Moved) && pl[x, 7].Type == ChessType.BlackRook)
                {
                    if (pl[x, y + 1].Type == ChessType.None && pl[x, y + 2].Type == ChessType.None)
                    {
                        if (!(pl[x, y].WhiteControl || pl[x, y + 1].WhiteControl || pl[x, y + 2].WhiteControl))
                        {
                            ls.Add(new XY(x, y + 2));
                        }
                    }
                }
            }
            #endregion
            return ls;
        }
        public List<XY> CanEn_passant(XY target)
        {
            int x = target.X;
            int y = target.Y;
            ChessOwner owner = pl[x, y].Owner;
            List<XY> ls = new List<XY>();
            if (!IsPawn(pl[x, y].Type)) return ls;
            if (owner == ChessOwner.White)
            {
                if (x != 4) return ls;
                if (y != 0)
                {
                    if (pl[x, y - 1].Type == ChessType.BlackPawn)
                    {
                        if (En_passantSub(new XY(x, y - 1)))
                        {
                            ls.Add(new XY(x + 1, y - 1));
                        }
                    }
                }
                if (y != 7)
                {
                    if (pl[x, y + 1].Type == ChessType.BlackPawn)
                    {
                        if (En_passantSub(new XY(x, y + 1)))
                        {
                            ls.Add(new XY(x + 1, y + 1));
                        }
                    }
                }
            }
            else if (owner == ChessOwner.Black)
            {
                if (x != 3) return ls;
                if (y != 0)
                {
                    if (pl[x, y - 1].Type == ChessType.WhitePawn)
                    {
                        if (En_passantSub(new XY(x, y - 1)))
                        {
                            ls.Add(new XY(x - 1, y - 1));
                        }
                    }
                }
                if (y != 7)
                {
                    if (pl[x, y + 1].Type == ChessType.WhitePawn)
                    {
                        if (En_passantSub(new XY(x, y + 1)))
                        {
                            ls.Add(new XY(x - 1, y + 1));
                        }
                    }
                }
            }
            return ls;
        }
        private bool En_passantSub(XY enemy)
        {
            int x = enemy.X;
            int y = enemy.Y;
            if (pl[x, y].Owner == ChessOwner.None) return false;
            if (LastChess != pl[x, y].Type) return false;
            if (LastMoveTo != enemy) return false;
            if (LastMove != new XY((pl[x, y].Owner == ChessOwner.White ? 1 : 6), y)) return false;
            return true;
        }
        public static bool IsKing(ChessType type)
        {
            if (type == ChessType.BlackKing || type == ChessType.WhiteKing) return true;
            else return false;
        }
        public static bool IsPawn(ChessType type)
        {
            if (type == ChessType.BlackPawn || type == ChessType.WhitePawn) return true;
            else return false;
        }
        public ChessBlock this[int x, int y]
        {
            get
            {
                return pl[x, y];
            }
        }
        public bool Upgrade(UpgradeType type)
        {
            if (Upgrading == ChessOwner.None) return false;
            pl[LastMoveTo.X, LastMoveTo.Y].Type -= (byte)type;
            Upgrading = ChessOwner.None;
            return true;
        }
        public bool Move(XY from, XY to)
        {
            if (pl[from.X, from.Y].Owner == ChessOwner.None) return false;

            if (Upgrading != ChessOwner.None) return false;
            ChessOwner upgradetmp = ChessOwner.None;

            #region Castling
            if (IsKing(pl[from.X, from.Y].Type))
            {
                foreach (XY item in CanCastling(from))
                {
                    if (item == to)
                    {
                        pl[to.X, to.Y].Type = pl[from.X, from.Y].Type;
                        pl[from.X, from.Y].Type = ChessType.None;
                        pl[from.X, (to.Y + from.Y) / 2].Type = pl[from.X, to.Y > from.Y ? 7 : 0].Type;
                        pl[from.X, to.Y > from.Y ? 7 : 0].Type = ChessType.None;

                        LastMove = from;
                        LastMoveTo = to;
                        LastChess = pl[to.X, to.Y].Type;
                        return true;
                    }
                }
            }
            #endregion

            #region En_passant
            if (IsPawn(pl[from.X, from.Y].Type))
            {
                foreach (XY item in CanEn_passant(from))
                {
                    if (item == to)
                    {
                        pl[from.X, to.Y].Type = ChessType.None;
                        pl[to.X, to.Y].Type = pl[from.X, from.Y].Type;
                        pl[from.X, from.Y].Type = ChessType.None;

                        LastMove = from;
                        LastMoveTo = to;
                        LastChess = pl[to.X, to.Y].Type;
                        return true;
                    }
                }
            }
            #endregion

            #region Upgrade
            if (pl[from.X, from.Y].Type == ChessType.WhitePawn && to.X == 7)
            {
                upgradetmp = ChessOwner.White;
            }
            if (pl[from.X, from.Y].Type == ChessType.BlackPawn && to.X == 0)
            {
                upgradetmp = ChessOwner.Black;
            }
            #endregion

            #region Normal
            foreach (XY item in SafeMoveSearch(from))
            {
                if (item == to)
                {
                    pl[to.X, to.Y].Type = pl[from.X, from.Y].Type;
                    pl[from.X, from.Y].Type = ChessType.None;
                    LastMove = from;
                    LastMoveTo = to;
                    LastChess = pl[to.X, to.Y].Type;
                    Upgrading = upgradetmp;
                    return true;
                }
            }
            #endregion
            return false;
        }
        private bool IsKingSafe(XY from, XY to)
        {
            ChessBlock[,] tmp = new ChessBlock[8, 8];
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    tmp[i, j] = (ChessBlock)pl[i, j].Clone();
                }
            }
            ChessOwner owner = tmp[from.X, from.Y].Owner;

            tmp[to.X, to.Y].Type = tmp[from.X, from.Y].Type;
            tmp[from.X, from.Y].Type = ChessType.None;

            RecalcSafe(tmp);
            if (owner == ChessOwner.Black)
            {
                foreach (ChessBlock item in tmp)
                {
                    if (item.Type == ChessType.BlackKing)
                    {
                        return !item.WhiteControl;
                    }
                }
                System.Diagnostics.Debug.Print("NoKing!!!");
                return true;
            }
            else if (owner == ChessOwner.White)
            {
                foreach (ChessBlock item in tmp)
                {
                    if (item.Type == ChessType.WhiteKing)
                    {
                        return !item.BlackControl;
                    }
                }
                System.Diagnostics.Debug.Print("NoKing!!!");
                return true;
            }
            throw new ArgumentException();
        }
        public List<XY> SafeMoveSearch(XY t)
        {
            int x = t.X;
            int y = t.Y;
            List<XY> ls = MoveSearch(t);
            List<XY> result = new List<XY>();
            RecalcSafe();
            foreach (XY target in ls)
            {
                if (IsKingSafe(t, target)) result.Add(target);
            }
            result.AddRange(CanCastling(t));
            return result;
        }
        private void RecalcSafe(ChessBlock[,] expl = null)
        {
            ChessBlock[,] tpanel = pl;
            if (expl != null) tpanel = expl;
            foreach (ChessBlock item in tpanel)
            {
                item.BlackControl = false;
                item.WhiteControl = false;
            }
            foreach (ChessBlock item in tpanel)
            {
                List<XY> ls = MoveSearch(new XY(item.x, item.y), true, tpanel);
                switch (item.Owner)
                {
                    case ChessOwner.White:
                        foreach (XY Pos in ls)
                        {
                            tpanel[Pos.X, Pos.Y].WhiteControl = true;
                        }
                        break;
                    case ChessOwner.Black:
                        foreach (XY Pos in ls)
                        {
                            tpanel[Pos.X, Pos.Y].BlackControl = true;
                        }
                        break;
                }
            }
        }
        private List<XY> MoveSearch(XY t, bool AttackOnly = false, ChessBlock[,] expl = null)
        {
            ChessBlock[,] tpanel = pl;
            if (expl != null) tpanel = expl;
            int x = t.X;
            int y = t.Y;
            List<XY> ls = new List<XY>();
            #region SearchCode
            switch (tpanel[x, y].Type)
            {
                case ChessType.BlackKing:
                case ChessType.WhiteKing:
                    if (CanMove(MoveCheck(x + 1, y + 1, tpanel[x, y].Owner, tpanel)))
                        ls.Add(new XY(x + 1, y + 1));
                    if (CanMove(MoveCheck(x + 1, y, tpanel[x, y].Owner, tpanel)))
                        ls.Add(new XY(x + 1, y));
                    if (CanMove(MoveCheck(x + 1, y - 1, tpanel[x, y].Owner, tpanel)))
                        ls.Add(new XY(x + 1, y - 1));
                    if (CanMove(MoveCheck(x, y + 1, tpanel[x, y].Owner, tpanel)))
                        ls.Add(new XY(x, y + 1));
                    if (CanMove(MoveCheck(x, y - 1, tpanel[x, y].Owner, tpanel)))
                        ls.Add(new XY(x, y - 1));
                    if (CanMove(MoveCheck(x - 1, y + 1, tpanel[x, y].Owner, tpanel)))
                        ls.Add(new XY(x - 1, y + 1));
                    if (CanMove(MoveCheck(x - 1, y, tpanel[x, y].Owner, tpanel)))
                        ls.Add(new XY(x - 1, y));
                    if (CanMove(MoveCheck(x - 1, y - 1, tpanel[x, y].Owner, tpanel)))
                        ls.Add(new XY(x - 1, y - 1));
                    break;
                case ChessType.BlackQueen:
                case ChessType.WhiteQueen:
                    // From Bishop
                    for (int i = 1; i < 8; i++)
                    {
                        if (MoveCheck(x + i, y + i, tpanel[x, y].Owner, tpanel) == MoveType.Move)
                        {
                            ls.Add(new XY(x + i, y + i));
                            continue;
                        }
                        if (MoveCheck(x + i, y + i, tpanel[x, y].Owner, tpanel) == MoveType.Attack)
                        {
                            ls.Add(new XY(x + i, y + i));
                            break;
                        }
                        break;
                    }
                    for (int i = 1; i < 8; i++)
                    {
                        if (MoveCheck(x - i, y + i, tpanel[x, y].Owner, tpanel) == MoveType.Move)
                        {
                            ls.Add(new XY(x - i, y + i));
                            continue;
                        }
                        if (MoveCheck(x - i, y + i, tpanel[x, y].Owner, tpanel) == MoveType.Attack)
                        {
                            ls.Add(new XY(x - i, y + i));
                            break;
                        }
                        break;
                    }
                    for (int i = 1; i < 8; i++)
                    {
                        if (MoveCheck(x + i, y - i, tpanel[x, y].Owner, tpanel) == MoveType.Move)
                        {
                            ls.Add(new XY(x + i, y - i));
                            continue;
                        }
                        if (MoveCheck(x + i, y - i, tpanel[x, y].Owner, tpanel) == MoveType.Attack)
                        {
                            ls.Add(new XY(x + i, y - i));
                            break;
                        }
                        break;
                    }
                    for (int i = 1; i < 8; i++)
                    {
                        if (MoveCheck(x - i, y - i, tpanel[x, y].Owner, tpanel) == MoveType.Move)
                        {
                            ls.Add(new XY(x - i, y - i));
                            continue;
                        }
                        if (MoveCheck(x - i, y - i, tpanel[x, y].Owner, tpanel) == MoveType.Attack)
                        {
                            ls.Add(new XY(x - i, y - i));
                            break;
                        }
                        break;
                    }

                    //From Rook

                    for (int i = x + 1; i < 8; i++)
                    {
                        if (MoveCheck(i, y, tpanel[x, y].Owner, tpanel) == MoveType.Move)
                        {
                            ls.Add(new XY(i, y));
                            continue;
                        }
                        if (MoveCheck(i, y, tpanel[x, y].Owner, tpanel) == MoveType.Attack)
                        {
                            ls.Add(new XY(i, y));
                            break;
                        }
                        break;
                    }
                    for (int i = x - 1; i >= 0; i--)
                    {
                        if (MoveCheck(i, y, tpanel[x, y].Owner, tpanel) == MoveType.Move)
                        {
                            ls.Add(new XY(i, y));
                            continue;
                        }
                        if (MoveCheck(i, y, tpanel[x, y].Owner, tpanel) == MoveType.Attack)
                        {
                            ls.Add(new XY(i, y));
                            break;
                        }
                        break;
                    }
                    for (int i = y + 1; i < 8; i++)
                    {
                        if (MoveCheck(x, i, tpanel[x, y].Owner, tpanel) == MoveType.Move)
                        {
                            ls.Add(new XY(x, i));
                            continue;
                        }
                        if (MoveCheck(x, i, tpanel[x, y].Owner, tpanel) == MoveType.Attack)
                        {
                            ls.Add(new XY(x, i));
                            break;
                        }
                        break;
                    }
                    for (int i = y - 1; i >= 0; i--)
                    {
                        if (MoveCheck(x, i, tpanel[x, y].Owner, tpanel) == MoveType.Move)
                        {
                            ls.Add(new XY(x, i));
                            continue;
                        }
                        if (MoveCheck(x, i, tpanel[x, y].Owner, tpanel) == MoveType.Attack)
                        {
                            ls.Add(new XY(x, i));
                            break;
                        }
                        break;
                    }
                    break;
                case ChessType.BlackBishop:
                case ChessType.WhiteBishop:
                    for (int i = 1; i < 8; i++)
                    {
                        if (MoveCheck(x + i, y + i, tpanel[x, y].Owner, tpanel) == MoveType.Move)
                        {
                            ls.Add(new XY(x + i, y + i));
                            continue;
                        }
                        if (MoveCheck(x + i, y + i, tpanel[x, y].Owner, tpanel) == MoveType.Attack)
                        {
                            ls.Add(new XY(x + i, y + i));
                            break;
                        }
                        break;
                    }
                    for (int i = 1; i < 8; i++)
                    {
                        if (MoveCheck(x - i, y + i, tpanel[x, y].Owner, tpanel) == MoveType.Move)
                        {
                            ls.Add(new XY(x - i, y + i));
                            continue;
                        }
                        if (MoveCheck(x - i, y + i, tpanel[x, y].Owner, tpanel) == MoveType.Attack)
                        {
                            ls.Add(new XY(x - i, y + i));
                            break;
                        }
                        break;
                    }
                    for (int i = 1; i < 8; i++)
                    {
                        if (MoveCheck(x + i, y - i, tpanel[x, y].Owner, tpanel) == MoveType.Move)
                        {
                            ls.Add(new XY(x + i, y - i));
                            continue;
                        }
                        if (MoveCheck(x + i, y - i, tpanel[x, y].Owner, tpanel) == MoveType.Attack)
                        {
                            ls.Add(new XY(x + i, y - i));
                            break;
                        }
                        break;
                    }
                    for (int i = 1; i < 8; i++)
                    {
                        if (MoveCheck(x - i, y - i, tpanel[x, y].Owner, tpanel) == MoveType.Move)
                        {
                            ls.Add(new XY(x - i, y - i));
                            continue;
                        }
                        if (MoveCheck(x - i, y - i, tpanel[x, y].Owner, tpanel) == MoveType.Attack)
                        {
                            ls.Add(new XY(x - i, y - i));
                            break;
                        }
                        break;
                    }
                    break;
                case ChessType.BlackKnight:
                case ChessType.WhiteKnight:
                    if (CanMove(MoveCheck(x + 2, y + 1, tpanel[x, y].Owner, tpanel)))
                        ls.Add(new XY(x + 2, y + 1));
                    if (CanMove(MoveCheck(x + 2, y - 1, tpanel[x, y].Owner, tpanel)))
                        ls.Add(new XY(x + 2, y - 1));

                    if (CanMove(MoveCheck(x + 1, y + 2, tpanel[x, y].Owner, tpanel)))
                        ls.Add(new XY(x + 1, y + 2));
                    if (CanMove(MoveCheck(x + 1, y - 2, tpanel[x, y].Owner, tpanel)))
                        ls.Add(new XY(x + 1, y - 2));

                    if (CanMove(MoveCheck(x - 1, y + 2, tpanel[x, y].Owner, tpanel)))
                        ls.Add(new XY(x - 1, y + 2));
                    if (CanMove(MoveCheck(x - 1, y - 2, tpanel[x, y].Owner, tpanel)))
                        ls.Add(new XY(x - 1, y - 2));

                    if (CanMove(MoveCheck(x - 2, y + 1, tpanel[x, y].Owner, tpanel)))
                        ls.Add(new XY(x - 2, y + 1));
                    if (CanMove(MoveCheck(x - 2, y - 1, tpanel[x, y].Owner, tpanel)))
                        ls.Add(new XY(x - 2, y - 1));
                    break;
                case ChessType.BlackRook:
                case ChessType.WhiteRook:
                    for (int i = x + 1; i < 8; i++)
                    {
                        if (MoveCheck(i, y, tpanel[x, y].Owner, tpanel) == MoveType.Move)
                        {
                            ls.Add(new XY(i, y));
                            continue;
                        }
                        if (MoveCheck(i, y, tpanel[x, y].Owner, tpanel) == MoveType.Attack)
                        {
                            ls.Add(new XY(i, y));
                            break;
                        }
                        break;
                    }
                    for (int i = x - 1; i >= 0; i--)
                    {
                        if (MoveCheck(i, y, tpanel[x, y].Owner, tpanel) == MoveType.Move)
                        {
                            ls.Add(new XY(i, y));
                            continue;
                        }
                        if (MoveCheck(i, y, tpanel[x, y].Owner, tpanel) == MoveType.Attack)
                        {
                            ls.Add(new XY(i, y));
                            break;
                        }
                        break;
                    }
                    for (int i = y + 1; i < 8; i++)
                    {
                        if (MoveCheck(x, i, tpanel[x, y].Owner, tpanel) == MoveType.Move)
                        {
                            ls.Add(new XY(x, i));
                            continue;
                        }
                        if (MoveCheck(x, i, tpanel[x, y].Owner, tpanel) == MoveType.Attack)
                        {
                            ls.Add(new XY(x, i));
                            break;
                        }
                        break;
                    }
                    for (int i = y - 1; i >= 0; i--)
                    {
                        if (MoveCheck(x, i, tpanel[x, y].Owner, tpanel) == MoveType.Move)
                        {
                            ls.Add(new XY(x, i));
                            continue;
                        }
                        if (MoveCheck(x, i, tpanel[x, y].Owner, tpanel) == MoveType.Attack)
                        {
                            ls.Add(new XY(x, i));
                            break;
                        }
                        break;
                    }
                    break;
                case ChessType.BlackPawn:
                    if (MoveCheck(x - 1, y, ChessOwner.Black, tpanel) == MoveType.Move && !AttackOnly)
                    {
                        ls.Add(new XY(x - 1, y));
                        if (!tpanel[x, y].Moved)//2StepsRule
                            if (MoveCheck(x - 2, y, ChessOwner.Black, tpanel) == MoveType.Move && !AttackOnly)
                                ls.Add(new XY(x - 2, y));
                    }
                    if (MoveCheck(x - 1, y - 1, ChessOwner.Black, tpanel) == MoveType.Attack)
                        ls.Add(new XY(x - 1, y - 1));
                    if (MoveCheck(x - 1, y + 1, ChessOwner.Black, tpanel) == MoveType.Attack)
                        ls.Add(new XY(x - 1, y + 1));
                    ls.AddRange(CanEn_passant(t));
                    break;
                case ChessType.WhitePawn:
                    if (MoveCheck(x + 1, y, ChessOwner.White, tpanel) == MoveType.Move && !AttackOnly)
                    {
                        ls.Add(new XY(x + 1, y));
                        if (!tpanel[x, y].Moved)//2StepsRule
                            if (MoveCheck(x + 2, y, ChessOwner.White, tpanel) == MoveType.Move && !AttackOnly)
                                ls.Add(new XY(x + 2, y));
                    }
                    if (MoveCheck(x + 1, y - 1, ChessOwner.White, tpanel) == MoveType.Attack)
                        ls.Add(new XY(x + 1, y - 1));
                    if (MoveCheck(x + 1, y + 1, ChessOwner.White, tpanel) == MoveType.Attack)
                        ls.Add(new XY(x + 1, y + 1));
                    ls.AddRange(CanEn_passant(t));
                    break;
            }
            #endregion
            return ls;
        }
        public MoveType MoveCheck(int x, int y, ChessOwner owner, ChessBlock[,] expl = null)
        {
            if (expl != null) return MoveCheck(new XY(x, y), owner, expl);
            else return MoveCheck(new XY(x, y), owner);
        }
        public MoveType MoveCheck(XY target, ChessOwner owner, ChessBlock[,] expl = null)
        {
            ChessBlock[,] tpanel = pl;
            if (expl != null) tpanel = expl;
            int x = target.X;
            int y = target.Y;
            if (x < 0 || x >= 8 || y < 0 || y >= 8)
                return MoveType.OutOfPanel;
            if (tpanel[x, y].Owner == ChessOwner.None)
                return MoveType.Move;
            if (tpanel[x, y].Owner == owner)
                return MoveType.Blocked;
            return MoveType.Attack;
        }
        public MoveType MoveDetect(XY from, XY target)
        {
            int fx = from.X;
            int fy = from.Y;
            int tx = target.X;
            int ty = target.Y;
            ChessOwner owner = pl[fx, fy].Owner;
            if (fx < 0 || fx >= 8 || fy < 0 || fy >= 8)
                return MoveType.OutOfPanel;
            if (tx < 0 || tx >= 8 || ty < 0 || ty >= 8)
                return MoveType.OutOfPanel;
            //Special moves detect
            if (IsKing(pl[fx, fy].Type))
            {
                foreach (XY item in CanCastling(from))
                {
                    if (item == target) return MoveType.Castling;
                }
            }
            if (IsPawn(pl[fx, fy].Type))
            {
                foreach (XY item in CanEn_passant(from))
                {
                    if (item == target) return MoveType.En_Passant;
                }
                if (pl[from.X, from.Y].Type == ChessType.WhitePawn && tx == 7)
                {
                    return MoveType.Upgrade;
                }
                if (pl[from.X, from.Y].Type == ChessType.BlackPawn && tx == 0)
                {
                    return MoveType.Upgrade;
                }
            }
            return MoveCheck(target, owner);
        }
        public static bool CanMove(MoveType type)
        {
            if (type == MoveType.Move || type == MoveType.Attack)
                return true;
            else
                return false;
        }
        public void DebugOutput()
        {
            for (int x = 0; x < 8; x++)
            {
                string line = "";
                for (int y = 0; y < 8; y++)
                {
                    switch (pl[x, y].Type)
                    {
                        case ChessType.None:
                            line += ".";
                            break;
                        case ChessType.BlackKing:
                            line += "$";
                            break;
                        case ChessType.BlackQueen:
                            line += "q";
                            break;
                        case ChessType.BlackBishop:
                            line += "b";
                            break;
                        case ChessType.BlackKnight:
                            line += "k";
                            break;
                        case ChessType.BlackRook:
                            line += "r";
                            break;
                        case ChessType.BlackPawn:
                            line += "p";
                            break;
                        case ChessType.WhiteKing:
                            line += "#";
                            break;
                        case ChessType.WhiteQueen:
                            line += "Q";
                            break;
                        case ChessType.WhiteBishop:
                            line += "B";
                            break;
                        case ChessType.WhiteKnight:
                            line += "K";
                            break;
                        case ChessType.WhiteRook:
                            line += "R";
                            break;
                        case ChessType.WhitePawn:
                            line += "P";
                            break;
                    }
                }
                System.Diagnostics.Debug.Print(line);
            }
        }
        public IEnumerator GetEnumerator()
        {
            return new ChessPanelEnumerator(pl);
        }
    }
    public class ChessPanelEnumerator : IEnumerator
    {
        int x = 0;
        int y = 0;
        private ChessBlock[,] p;
        internal ChessPanelEnumerator(ChessBlock[,] pl)
        {
            p = pl;
        }
        ChessBlock Current
        {
            get
            {
                return p[x, y];
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        bool IEnumerator.MoveNext()
        {
            y++;
            if (y >= 8)
            {
                y -= 8;
                x--;
                if (x < 0)
                {
                    return false;
                }
            }
            return true;
        }

        void IEnumerator.Reset()
        {
            x = 7;
            y = 0;
        }
    }
}
